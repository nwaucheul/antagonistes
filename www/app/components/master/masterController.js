app.controller('masterController', function($scope, $rootScope, Locations, Items, Game){
	
	$scope.game = Game.getCurrentGame();
	$scope.locations = null;
	$scope.min = 25;
	$scope.max = 50;
	$scope.items = null;
	$(document).scrollTop(0);
	var getLocations = function(){
		Locations.getLocations()
			.then(function(data){
				$scope.locations = data;
				$scope.$safeApply();
			}
		);
	};
	$scope.getItems = function(){
		Items.getItems($scope.min, $scope.max, $scope.selectedLocation)
			.then(function(data){
				$scope.items = data;
			}
		);
	};
	$scope.updateLocation = function(){
		$rootScope.$emit('updateLocation', $scope.selectedLocation);
	}
	getLocations();

});