app.controller('lootListController', function($scope, $location, Game, Items){
	$(document).scrollTop(0);
	$scope.edit = 0;
	$scope.lootGeneration = null;
	if(Game.getCurrentGame() == undefined){
		$location.path('/home/');
	}
	$scope.game = Game.getCurrentGame();
	$scope.access = Game.getAccess();
	$scope.lootList = null;
	$scope.gameName = null;
	if($scope.game){
		Items.getItemLists($scope.game[0].id)
			.then(function(data){
				$scope.lootList = data;
		});
	}
	$scope.editLoot = function(id){
		Items.setCurrentItemList(id);
		if($scope.access === 'master'){
			clearInterval(interval);
			$location.path('/master/');
		}
		else{
			clearInterval(interval);
			$location.path('/client/');
		}
	}

	$scope.updateGameName = function(){
		Game.updateGameName($scope.gameName).then(function(data){
			Game.getGame($scope.game[0].code_players).then(function(data){
				$scope.game = Game.getCurrentGame();
			});
		});
	}

	$scope.createNewloot = function(){
		if($scope.game == undefined){
			clearInterval(interval);
			$location.path("/home");
			return;
		}
		Items.resetCurrentItemList();
		clearInterval(interval);
		$location.path('/master');
	}

	function listRefresh(){
		var gameId = Game.getCurrentGame();
		Items.getItemLists(gameId[0].id)
				.then(function(data){
					$scope.lootList = data;
				});
	}

	$scope.editListName = function(){
		if($scope.access == 'master'){
			$scope.edit = 1;
		}
	}
	var interval = setInterval(listRefresh, 5000);


});