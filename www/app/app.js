/*document.addEventListener("deviceready", function () {
	angular.bootstrap(document, ['app']);
}, true);*/

var app = angular.module("app", ['SafeApply', 'ngCordova', 'ngRoute' ,'ngAnimate']);

app.config(function($routeProvider){
	$routeProvider
		.when('/home', {
			templateUrl: 'app/components/home/home.html',
			controller: 'homeController'
		})
		.when('/client/', {
			templateUrl: 'app/components/client/client.html',
			controller: 'clientController'
		})
		.when('/master', {
			templateUrl: 'app/components/master/master.html',
			controller: 'masterController'
		})
		.when('/lootList', {
			templateUrl: 'app/components/lootList/lootList.html',
			controller: 'lootListController'
		})
		.otherwise({redirectTo: '/home'});
});