app.directive('lootForm', function($location, Items, Game, $rootScope, $safeApply){

	return {
		restrict: 'E',
		replace: 'true',
		scope: {
			location: '=',
			items: '=',
			id: '='
		},
		templateUrl: 'app/shared/directives/templates/lootFormTemplate.html',
		link: function(scope, element, attrs){
			scope.accessType = Game.getAccess();
			scope.editTitle = 0;
			scope.itemDescription = null;
			scope.itemDetailToShow = null;
			scope.newName = "Nom";
			scope.newNumber = 1;

			var edit = false;
			$rootScope.$on('updateLocation', function(e, selectedLocation){
				if(scope.friendly_name == undefined || scope.friendly_name ==""){
					scope.friendly_name = selectedLocation;
				}
			});
			if(Items.getCurrentItemList() !== null){
				edit = true;
				Items.getItemList(Items.getCurrentItemList()).then(function(data){
					scope.items = data;
					scope.visible = Items.isActiveList();
					if(scope.visible == 1){
						scope.visible = "Oui";
					}
					else{
						scope.visible = "Non";
					}
					scope.friendly_name = Items.getFriendlyName();
				});
			}
			scope.visible = Items.isActiveList();
			scope.saveItemList = function(){
				if(scope.id == undefined){
					$location.path("/home");
					return;
				}
				if(scope.friendly_name == undefined){
					scope.friendly_name = scope.location;
				}
				if(scope.visible =="Oui"){
					scope.visible = 1;
				}
				else{
					scope.visible = 0;
				}
				Items.saveItemList(edit, scope.id, scope.friendly_name, scope.location, JSON.stringify(scope.items), scope.visible)
				.then(function(data){
					if (data) {
						$location.path('/lootList/');
					}
				});
			};
			scope.editListName = function(){
				if(scope.accessType == 'master'){
					scope.editTitle = 1;
				}
			}
			scope.itemDetails = function(itemId, desc){
				if(desc){
					return;
				}
				Items.getItemDescription(itemId).then(function(data){
					scope.itemDescription = data[0].value;
					scope.itemTaille = data[1].value;
					scope.itemResistance = data[2].value;
					scope.itemValeur = data[3].value;
					scope.itemMaitrise = data[4].value;
					scope.itemDetailToShow = itemId;
				});
			}
			scope.addItem = function(){
				var tempItem = new Array();
				tempItem.push(Math.floor(Math.random()*10000));
				tempItem.push(scope.newName);
				tempItem.push(scope.newNumber);
				scope.items.push(tempItem);
				scope.newName = "Nom";
				scope.newNumber = 1;
				scope.itemShow = 0;
			}
			$(document).scrollTop(0);

		}
	}

});
