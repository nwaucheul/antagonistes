app.service('Game', function($q, $http){
	var game = null;
	var accessType = 'master';

	this.createGame = function(){
		var url = 'http://antagonistes.com/tools/angularApp/gameAPI.php?createGame=true&friendly_name=Nouvelle%20partie';
		var def = $q.defer();
		$http.jsonp(url + '&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
			});
			
			return def.promise;
	};

	this.updateGameName = function(name){
		console.log(game);
		var url = 'http://antagonistes.com/tools/angularApp/gameAPI.php?updateGameName=true&friendly_name='+encodeURIComponent(name)+'&id='+game[0].id;
		var def = $q.defer();
		$http.jsonp(url + '&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
			});
			
			return def.promise;
	};

	this.getGame = function(input){
		var url = 'http://antagonistes.com/tools/angularApp/gameAPI.php?getGame=true&input='+input;
		var def = $q.defer();
		$http.jsonp(url + '&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
				game = data;
			});
		return def.promise;
	};

	this.setAccess = function(access){
		accessType = access;
	}

	this.getAccess = function(){
		return accessType;
	}

	this.getCurrentGame = function(){
		return game;
	}

	this.setCurrentGame = function(newGame){
		game = newGame;
	}

});