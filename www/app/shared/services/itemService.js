app.service('Items', function($q, $http, $location){
	var currentItemList = null;
	var activeList = null;
	var friendly_name = null;

	this.getItems = function(min, max, location){
		var url = 'http://antagonistes.com/tools/angularApp/lootGenAPI.php?getItems=true';
		var def = $q.defer();
		console.log(location);
		$http.jsonp( url + '&min='+ parseInt(min) + '&max=' + parseInt(max) + '&locations=' + location +'&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
			});
		return def.promise;
	};

	this.saveItemList = function(edit, id, friendly_name, location, items, active){
		activeList = active;
		if(!edit){
			var url = 'http://antagonistes.com/tools/angularApp/gameAPI.php?saveLootList=true&id='+id+'&friendly_name='+encodeURIComponent(friendly_name)+'&location='+location+'&items='+items+'&active='+active;
		}
		else{
			var url = 'http://antagonistes.com/tools/angularApp/gameAPI.php?editLootList=true&id='+currentItemList+'&friendly_name='+encodeURIComponent(friendly_name)+'&location='+location+'&items='+items+'&active='+active;
		}
		var def = $q.defer();
		$http.jsonp(url + '&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
			});
		return def.promise;
	};

	this.getItemList = function(id){
		var url = 'http://antagonistes.com/tools/angularApp/gameAPI.php?getItemList=true&id='+id;
		var def = $q.defer();
		$http.jsonp( url + '&callback=JSON_CALLBACK')
			.success(function(data){
				activeList = data[0].active;
				friendly_name = data[0].friendly_name;
				var result = JSON.parse(data[0].items);
				def.resolve(result);
			});
		return def.promise;
	}

	this.getItemLists = function(id){
		var url = 'http://antagonistes.com/tools/angularApp/gameAPI.php?getLootLists=true&id='+id;
		var def = $q.defer();
		$http.jsonp(url + '&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
			});
		return def.promise;
	};

	this.isActiveList = function(){
		return activeList;
	}

	this.getFriendlyName = function(){
		return friendly_name;
	}

	this.setCurrentItemList = function(id){
		currentItemList = id;
	}

	this.getCurrentItemList = function(){
		return currentItemList;
	}

	this.resetCurrentItemList = function(){
		currentItemList = null;
	}
	this.getItemDescription = function(itemId){
		var url = "http://antagonistes.com/tools/angularApp/lootGenAPI.php?getItemDescription=true&itemId="+itemId;
		var def = $q.defer();
		$http.jsonp(url + '&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
			});
		return def.promise;
	}

});
