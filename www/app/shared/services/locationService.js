app.service('Locations', function($q, $http){
	var url = 'http://antagonistes.com/tools/angularApp/lootGenAPI.php?location=true';
	this.getLocations = function(){
		var def = $q.defer();
		$http.jsonp( url + '&callback=JSON_CALLBACK')
			.success(function(data){
				def.resolve(data);
			});
		return def.promise;
	};

});
