app.controller('homeController', function($scope, $location, Game, Items){

	$scope.resetForm = function(){
		$scope.showForm = false;
		$scope.game = null;
		$scope.playerInput = null;
		$scope.successCreate = false;
		$scope.newGame = false;
		Game.setCurrentGame(null);
		Items.resetCurrentItemList();
		$(document).scrollTop(0);


	}

	$scope.createGame = function(){
		Game.createGame($scope.gameName)
			.then(function(data){
				$scope.successCreate = true;
				$scope.game = data;
				$scope.newGame = true;
				$scope.playerInput = $scope.game['masterCode'];
			});
	}

	$scope.loadGame = function(){
		if($scope.playerInput !== null){
			Game.getGame($scope.playerInput)
				.then(function(data){
					if(data.length !== 0){
						if($scope.newGame === true){
							$location.path('/master/');
						}
						else if($scope.playerInput === data[0].code_master){
							Game.setAccess('master');
							$location.path('/lootList/');
						}
						else{
							Game.setAccess('client');
							$location.path('/lootList/');
						}
					}
				})
		}
	};

	$scope.resetForm();

});